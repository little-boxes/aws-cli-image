FROM debian:stable

# Update and install packages
RUN apt-get update && \
  apt-get install -y curl gpg unzip

# Download and verify AWS ClI
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip.sig" -o "awscliv2.sig"
COPY aws-pub-key aws-pub-key
RUN gpg --import aws-pub-key && \
  gpg --verify awscliv2.sig awscliv2.zip && \
  unzip awscliv2.zip && \
  ./aws/install
